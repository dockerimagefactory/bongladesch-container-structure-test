#!/bin/bash
set -eux

# rename all *.yml files to *.yaml if exists
if [ $(ls /test-config/*.yml | wc -l) -gt 0 ]
then
    cd /test-config/
    mmv -d \*.yml \#1.yaml
    cd /
fi

# run all tests
container-structure-test -test.v -image $1 /test-config/*.yaml
