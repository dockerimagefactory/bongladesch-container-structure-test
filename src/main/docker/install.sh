#!/bin/bash
set -eux

apt-get update -y
apt-get install -y curl mmv
curl -LO https://storage.googleapis.com/container-structure-test/v${version}/container-structure-test
chmod +x container-structure-test
mv container-structure-test /usr/local/bin/
apt-get autoremove -y curl
mkdir /test-config
